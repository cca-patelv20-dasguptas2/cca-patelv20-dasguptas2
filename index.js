const express = require('express')
const app = express()
var port = process.env.PORT || 8080;
const bodyParser = require("body-parser");
const request = require('request')
app.use(express.urlencoded({ extended: false }))
const cors = require('cors') //New for microservice
app.use(bodyParser.json());
app.use(cors())

app.listen(port)
console.log("Express server is running on port " + port)
app.get("/", (req, res) => {
    res.send("Microservice Gateway by Vatsa Patel and San Dasgupta Usage:/Color_Combination?color=")
})
// app.get('/Color_Combination', function (req, res) {
//     var result = req.query.color.toLowerCase()
//     switch (result) {
//         case 'black':
//             result = 'Combination is R =0, G = 0, B = 0'
//             break;
//         case 'white':
//             result = 'Combination is R = 255, G = 255, B = 255'
//             break;
//         case 'red':
//             result = 'Combination is R = 255, G = 0, B = 0'
//             break;
//         case 'green':
//             result = 'Combination is R = 0, G = 255, B = 0'
//             break;
//         case 'blue':
//             result = 'Combination is R = 0, G = 0, B = 255'
//             break;
//         case 'yellow':
//             result = 'Combination is R = 255, G = 255, B = 0'
//             break;
//         case 'cyan':
//             result = 'Combination is R = 0, G = 255, B = 255'
//             break;
//         case 'magenta':
//             result = 'Combination is R = 255, G = 0, B = 255'
//             break;
//         case 'silver':
//             result = 'Combination is R = 192, G = 192, B = 192'
//             break;
//         case 'gray':
//             result = 'Combination is R = 128, G = 128, B = 128'
//             break;
//         case 'olive':
//             result = 'Combination is R = 128, G = 128, B = 0'
//             break;
//         case 'purple':
//             result = 'Combination is R = 128, G = 0, B = 128'
//             break;
//         case 'teal':
//             result = 'Combination is R = 0, G = 128, B = 128'
//             break;
//         case 'navy':
//             result = 'Combination is R = 0, G = 0, B = 128'
//             break;


//         default:
//             result = `We donot have the combination of ${result}.`;
//     }
//     res.send(result)
// })

app.post('/Color_Combination', function (req, res) {
    console.log("req.body >>>",req.body);
    var data = req.body;
    var result 
    if(data && data.color)
        result= data.color.toLowerCase();
    console.log(":::::::::::::::::::::::::::::::::::::::", req.body.color);
    switch (result) {
        case 'black':
            result = 'Combination is R =0, G = 0, B = 0'
            break;
        case 'white':
            result = 'Combination is R = 255, G = 255, B = 255'
            break;
        case 'red':
            result = 'Combination is R = 255, G = 0, B = 0'
            break;
        case 'green':
            result = 'Combination is R = 0, G = 255, B = 0'
            break;
        case 'blue':
            result = 'Combination is R = 0, G = 0, B = 255'
            break;
        case 'yellow':
            result = 'Combination is R = 255, G = 255, B = 0'
            break;
        case 'cyan':
            result = 'Combination is R = 0, G = 255, B = 255'
            break;
        case 'magenta':
            result = 'Combination is R = 255, G = 0, B = 255'
            break;
        case 'silver':
            result = 'Combination is R = 192, G = 192, B = 192'
            break;
        case 'gray':
            result = 'Combination is R = 128, G = 128, B = 128'
            break;
        case 'olive':
            result = 'Combination is R = 128, G = 128, B = 0'
            break;
        case 'purple':
            result = 'Combination is R = 128, G = 0, B = 128'
            break;
        case 'teal':
            result = 'Combination is R = 0, G = 128, B = 128'
            break;
        case 'navy':
            result = 'Combination is R = 0, G = 0, B = 128'
            break;


        default:
            result = "No color";
    }
    res.send({ error: null, data: result });

})